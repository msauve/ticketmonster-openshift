#!/usr/bin/env ruby
 
# Only tags >= min_tag will be removed.
min_tag = '0.0.0'
tags = `git tag`.split(/\s+/)
 
`git tag -d #{tags.join ' '}`
`git push origin #{tags.map { |t| ':' + t }.join ' '}`
